# Intro
This is just a basic docker image based on Debian 8.4 but with a lot of the cruft removed to keep it lightweight.
Designed for daily building with the latest security updates.

# Locked-in assumptions
This really started for my own use but is currently locked to English locale and Europe/London timezone.
Easily tweaked if you want to change things.

# Building
Running this will build the Dockerfile and tag the image with today's date.
Up to you if you want to tag it as latest.
```
docker build -t userzero/base-debian:8.4-"$(date +'%Y%m%d')" .
docker tag -f userzero/base-debian:8.4-"$(date +'%Y%m%d')" userzero/base-debian:latest
```
